/** @type {import('tailwindcss').Config} */
module.exports = {
	content: [
		'./index.html',
		'./src/**/*.{js,ts,jsx,tsx}',
		'./src/styles/globals.css',
	],
	theme: {
		extend: {},
	},
	plugins: [],
};
