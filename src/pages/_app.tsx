import { type ReactNode, Fragment } from 'react';

interface AppProps {
	children: ReactNode;
}

export default function App({ children }: AppProps): JSX.Element {
	return <Fragment>{children}</Fragment>;
}
