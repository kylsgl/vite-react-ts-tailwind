import { useState } from 'react';
import reactLogo from '../assets/react.svg';
import viteLogo from '/vite.svg';
import styles from './index.module.css';

export default function HomePage(): JSX.Element {
	const [count, setCount] = useState(0);

	const handleClick = (): void => {
		setCount((current: number): number => current + 1);
	};

	return (
		<div className="text-center">
			<div className="flex flex-row justify-center">
				<a href="https://vitejs.dev" target="_blank">
					<img alt="Vite logo" className={styles.logo} src={viteLogo} />
				</a>
				<a href="https://react.dev" target="_blank">
					<img
						alt="React logo"
						className={`${styles.logo} ${styles.react}`}
						src={reactLogo}
					/>
				</a>
			</div>
			<h1>Vite + React</h1>
			<div className={styles.card}>
				<button onClick={handleClick}>count is {count}</button>
				<p>
					Edit <code>src/App.tsx</code> and save to test HMR
				</p>
			</div>
			<p className={styles['read-the-docs']}>
				Click on the Vite and React logos to learn more
			</p>
		</div>
	);
}
