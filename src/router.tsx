import { Fragment } from 'react';
import {
	type RouteObject,
	createBrowserRouter,
	RouterProvider,
} from 'react-router-dom';

interface Page {
	path: string;
	element: JSX.Element;
}

type PageFiles = Record<string, { default: () => JSX.Element }>;

const pageFiles: PageFiles = import.meta.glob('/src/pages/**/[a-z[]*.tsx', {
	eager: true,
});

const reservedPageFiles: PageFiles = import.meta.glob(
	'/src/pages/(_app|404).tsx',
	{
		eager: true,
	}
);

const pages: Page[] = Object.keys(pageFiles).map((route): Page => {
	const path: string = route
		.replace(/\/src\/pages|index|\.tsx$/gi, '')
		.replace(/\[\.{3}.+\]/, '*')
		.replace(/\[(.+)\]/, ':$1');

	const Component = pageFiles[route].default;

	return { path, element: <Component /> };
});

const reservedPages: Record<string, (() => JSX.Element) | undefined> =
	Object.keys(reservedPageFiles).reduce((res, file) => {
		const key: string = file.replace(/\/src\/pages\/|\.tsx$/g, '');

		return { ...res, [key]: reservedPageFiles[file].default };
	}, {});

const router = createBrowserRouter(
	pages.map((page: Page): RouteObject => {
		const ErrorComponent = reservedPages['404'] ?? Fragment;

		return {
			...page,
			errorElement: <ErrorComponent />,
		};
	})
);

export default function Router(): JSX.Element {
	// eslint-disable-next-line no-underscore-dangle
	const AppComponent = reservedPages._app ?? Fragment;

	return (
		<AppComponent>
			<RouterProvider router={router} />
		</AppComponent>
	);
}
