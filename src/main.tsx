import { StrictMode } from 'react';
import ReactDOM from 'react-dom/client';

import './styles/globals.css';

import Router from './router';

// eslint-disable-next-line @typescript-eslint/no-non-null-assertion
ReactDOM.createRoot(document.getElementById('root')!).render(
	<StrictMode>
		<Router />
	</StrictMode>
);
